<?php

// includes
require_once '/data/create_db.php';
require_once '/data/db.php';

create_db();
//create database
?>
  

<!DOCTYPE html>
<html lang="ru" >
    <head>
        <meta charset="utf-8" />
        <title>Guestbook</title>
        <link href="css/jquery.fancybox.css" rel="stylesheet" type="text/css" />
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <link href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://jqueryvalidation.org/files/demo/site-demos.css">
        <script src= "js/jquery.min.js"></script>
        <script src="js/common.js"></script>
        <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
        <script src="js/jquery.fancybox.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/additional-methods.min.js"></script>
    </head>
    <body>
        <div class="btn-form">
           <a href="#modal-form" rel="nofollow" class="modalbox">Leave message</a>
        </div>
        <div id="modal-form">
        <form action="/data/insert.php" method="POST" id="guest-form" name="reg">
    <input id="username" type="text" name="name" placeholder="Username">
    <input id="email" type="text" name="email" placeholder="E-mail">
    <textarea id="message" type="text" name="text" placeholder="Your message"></textarea>
    <img src="data/captcha.php" alt="Картинка" /><br />
    <input id ="captcha" type="text" name="captcha" placeholder="Verify code" /><br />

    <input id="submit" class="btn btn-default" type="button" value="Submit">

</form>
</div>
    </body>
</html>
<?php
    $connect_db=mysql_connect(HOST, MYSQL_USER, MYSQL_PASS)   
    or die("No connection with SQL"); 
     mysql_select_db("guests_db",$connect_db);
    // Установка кодировки соединения
    mysql_query("SET NAMES 'utf8'",$connect_db);
    $result_sel = "select * from  book" or die(mysql_error()); 
    $res = mysql_query($result_sel);
?>

<table id="myTable">
    <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Message</th>
        </tr>
    </thead>
    <tbody>
             <?php while ($row = mysql_fetch_array($res)) {
      $username = $row['username'];
      $email = $row['email'];
      $message = $row["message"];
   echo '<tr>
            <td>'.$username.'</td>
            <td>'.$email.'</td>
            <td>'.$message.'</td>
        </tr>';
             }
      ?>
    </tbody>
</table>
        <script type="text/javascript">
       $(document).ready(function() {
            $('#submit').click(function() {
                var username = $('#username').val();
                var email = $('#email').val();
                var message = $('#message').val();
                var captcha = $('#captcha').val();
                if(username === ''){
                    alert('Please input data in all fields');
                }
                else {
                $.ajax({
                    type: "POST",
                    cache: false,        
                    url: '/data/insert.php',
                    data: {username: username, email: email, message: message, captcha: captcha},
                    success: function(response) {
                        var res = JSON.parse(response);
                        if('success' in res){
                             $("#guest-form").fadeOut("fast", function(){
                                $(this).before("<p><strong>Your message has been sent</strong></p>");
                                setTimeout("$.fancybox.close()", 1000);
                            });
                        }
                        else{
                                $('#guest-form').before("<p><strong>Captcha error, please try again</strong></p>");
                        }
                    }
                });
            }
            });
        });
        
       $(document).ready(function(){

    $("#guest-form").validate({

       rules:{

            name:{
                required: true,
                lettersonly: true
            },

            email:{
                email: true,
                required: true
            },
            text: {
                required: true
       },
       captcha: {
           required: true
       }
       

       }

    });
jQuery.validator.setDefaults({
  debug: true,
  success: "valid"
});
var form = $( "#guest-form" );
form.validate();
});

    </script>

    
