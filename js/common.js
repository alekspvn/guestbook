//Tables

$(document).ready(function(){
    $('#myTable').DataTable( {
       "pageLength": 25,
        "searching": false
    });
});

//Modal fancybox
$(document).ready(function(){
    $(".modalbox").fancybox();
    $("#guest-form").submit(function(){ return false; });
    $("#submit").on("click", function(){
         
        // тут дальнейшие действия по обработке формы
        // закрываем окно, как правило делать это нужно после обработки данных
        $("#guest-form").fadeOut("fast", function(){
            $(this).before("<p><strong>Your message has been sent</strong></p>");
            setTimeout("$.fancybox.close()", 1000);
        });
    });
});
